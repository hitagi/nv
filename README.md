# Neovim Configuration

## Installation

### Dependency

1. if possible use the latest version of neovim
2. [ctags](https://github.com/universal-ctags/ctags), dependency of tagbar
   plugins
3. git
4. curl
5. unzip
6. tar
7. gzip
8. ripgrep
9. fd
10. gcc
11. clang
12. make
13. node

install packer, plugin manager for Neovim:

```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

then, clone this repo to the path.

```
git clone --depth 1 https://codeberg.org/hitagi/nv.git\
  ~/.config/nvim
```

now open your neovim, then sync all plugins:

```
:PackerSync
```

## Check Healt

```
:checkhealt
```

done.
