require('code_runner-nvim') -- run simple code with keys
require('comment-nvim') -- comment plugins
require('nvim-keybind') -- configuration keybindings
require('nvim-lualine') -- status line plugins
require('nvim-scrollbar') -- scrollbar plugins
require('nvim-treesitter-config') -- highlighting syntax
require('pantran-nvim') -- translate plugins
require('plugins') -- all plugins compiled in here
require('toggleterm-nvim') -- virtual term
require('tokyonight-nvim') -- colorscheme
require('barbar-nvim') -- tabline plugins
require('nvim-ops') -- user options
require('neo-tree-nvim') -- tree based file explorer
require('nvim-cmp') -- completion
require('luasnip-config') -- snippets
require('codeium-nvim') -- ai plugins
require('neoformat') -- autoformatting plugins
require('ale') -- code linting plugins
require('lspkind-config') -- pictogram plugins
require('nvim-colorizer')
require('indent-blankline-nvim') -- adds indentation guides to all lines
require('template-nvim') -- nvim template plugins
require('telescope-config') -- find, filter, preview and picker plugins
