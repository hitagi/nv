vim.g.ale_linters = {
  cpp = { "clang" },
  c = { "clang" },
  html = { "eslint" },
  javascript = { "eslint" }
}

vim.g.ale_fixers = {
  cpp = { "clang-format" },
  c = { "clang-format" },
  javascript = { "eslint" }
}
