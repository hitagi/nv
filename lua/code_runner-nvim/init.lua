require('code_runner').setup({
  -- put here the commands by filetype
  -- choose default mode (valid term, tab, float, toggle, buf)
  mode = 'term',
  term = {
	position = "bot",
	size = 7,
  },
  filetype = {
		java = "cd $dir && javac $fileName && java $fileNameWithoutExt",
		python = "python3 -u",
		typescript = "deno run",
		rust = "cd $dir && rustc $fileName && $dir/$fileNameWithoutExt",
		cpp = "cd $dir && clang++ $fileName -o $fileNameWithoutExt && $dir/$fileNameWithoutExt",
		c = "cd $dir && clang $fileName -o $fileNameWithoutExt && $dir/$fileNameWithoutExt",
		go = "go run $dir/$fileName",
		javascript = "node $dir/$fileName"
	},
})
