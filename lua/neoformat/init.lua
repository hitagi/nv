vim.g.neoformat_cpp_clangformat = {
  exe = "clang-format",
  args = { "--style LLVM" }
}

vim.g.neoformat_c_clangformat = {
  exe = "clang-format",
  args = { "--style LLVM" }
}

vim.g.neoformat_html_htmlbeautify = {
  exe = "js-beautify",
  args = { "--type html", "--indent-size 2" },
  stdin = true
}

vim.g.neoformat_css_cssbeautify = {
  exe = "js-beautify",
  args = { "--type css", "--indent-size 2" },
  stdin = true
}

vim.g.neoformat_enabled_cpp = { "clangformat" }
vim.g.neoformat_enabled_c = { "clangformat" }
vim.g.neoformat_enabled_html = { "htmlbeautify" }
vim.g.neoformat_enabled_css = { "cssbeautify" }

local augroup = vim.api.nvim_create_augroup('fmt', {clear = true})

vim.api.nvim_create_autocmd('BufWritePre', {
  pattern = '*',
  group = augroup,
  command = 'undojoin | Neoformat'
})
