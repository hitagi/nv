-- reusable variable
local map = vim.keymap.set
local truesilent = { noremap = true, silent = true }
local nosilent = { noremap = true, silent = false }

local has_words_before = function()
  unpack = unpack or table.unpack
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local feedkey = function(key, mode)
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
end

-- auto parser
map('i', '{', '{}<Esc>ha')
map('i', '(', '()<Esc>ha')
map('i', '[', '[]<Esc>ha')
map('i', '"', '""<Esc>ha')
vim.cmd[[inoremap ' ''<Esc>ha]]
map('i', '`', '``<Esc>ha')

-- select all text
map('n', 'aa', '<Esc>ggvG<CR>', truesilent)

-- code runner
map('n', '<leader>r', '<Cmd>RunCode<CR>', nosilent)
map('n', '<leader>rf', '<Cmd>RunFile<CR>', nosilent)
map('n', '<leader>rft', '<Cmd>RunFile tab<CR>', nosilent)
map('n', '<leader>rp', '<Cmd>RunProject<CR>', nosilent)
map('n', '<leader>rc', '<Cmd>RunClose<CR>', nosilent)
map('n', '<leader>crf', '<Cmd>CRFiletype<CR>', nosilent)
map('n', '<leader>crp', '<Cmd>CRProjects<CR>', nosilent)

-- pantran
map('n', 'TR', '<Cmd>Pantran<CR>')

-- neo tree
map('n', 'f', '<Cmd>Neotree left<CR>')
map('i', '<C-f>', '<Esc><Cmd>Neotree left<CR>')
map('n', 'ff', ':Neotree left dir=')

-- toggle term
map('n', 'TT', '<Cmd>ToggleTerm size=40 direction=float<CR>')
map('n', 'Tl', '<Cmd>ToggleTerm size=40 direction=vertical<CR>')
map('n', 'Tb', '<Cmd>ToggleTerm size=10 direction=horizontal<CR>')

-- barbar
---- move to previous/next
map('n', '<A-,>', '<Cmd>BufferPrevious<CR>', truesilent)
map('n', '<A-.>', '<Cmd>BufferNext<CR>', truesilent)
---- re-order to previous/next
map('n', '<A-<>', '<Cmd>BufferMovePrevious<CR>', truesilent)
map('n', '<A->>', '<Cmd>BufferMoveNext<CR>', truesilent)
---- goto buffer in position...
map('n', '<A-1>', '<Cmd>BufferGoto 1<CR>', truesilent)
map('n', '<A-2>', '<Cmd>BufferGoto 2<CR>', truesilent)
map('n', '<A-3>', '<Cmd>BufferGoto 3<CR>', truesilent)
map('n', '<A-4>', '<Cmd>BufferGoto 4<CR>', truesilent)
map('n', '<A-5>', '<Cmd>BufferGoto 5<CR>', truesilent)
map('n', '<A-6>', '<Cmd>BufferGoto 6<CR>', truesilent)
map('n', '<A-7>', '<Cmd>BufferGoto 7<CR>', truesilent)
map('n', '<A-8>', '<Cmd>BufferGoto 8<CR>', truesilent)
map('n', '<A-9>', '<Cmd>BufferGoto 9<CR>', truesilent)
map('n', '<A-0>', '<Cmd>BufferLast<CR>', truesilent)
---- pin/unpin buffer
map('n', '<A-p>', '<Cmd>BufferPin<CR>', truesilent)
---- close buffer
map('n', '<A-c>', '<Cmd>BufferClose<CR>', truesilent)
---- magic buffer-picking mode
map('n', '<C-p>', '<Cmd>BufferPick<CR>', truesilent)
---- sort automatically by...
map('n', '<Space>bb', '<Cmd>BufferOrderByBufferNumber<CR>', truesilent)
map('n', '<Space>bd', '<Cmd>BufferOrderByDirectory<CR>', truesilent)
map('n', '<Space>bl', '<Cmd>BufferOrderByLanguage<CR>', truesilent)
map('n', '<Space>bw', '<Cmd>BufferOrderByWindowNumber<CR>', truesilent)

-- tagbar
map('n', '<Leader>t', '<Cmd>TagbarToggle<CR>', truesilent)

-- telescope
map('n', '<Leader>ff', require('telescope.builtin').find_files, {})
map('n', '<Leader>fg', require('telescope.builtin').live_grep, {})
map('n', '<Leader>fb', require('telescope.builtin').buffers, {})
map('n', '<Leader>fh', require('telescope.builtin').help_tags, {})

---- specific plugin

-- neo tree
require('neo-tree').setup({
  window = {
    position = "left",
    width = 35,
    mapping_options = {
      noremap = true,
      nowait = true,
    },
    mappings = {
      ["<space>"] = { 
          "toggle_node", 
          nowait = false, -- disable `nowait` if you have existing combos starting with this char that you want to use 
      },
      ["<2-LeftMouse>"] = "open",
      ["<cr>"] = "open",
      ["<esc>"] = "revert_preview",
      ["P"] = { "toggle_preview", config = { use_float = true } },
      ["l"] = "focus_preview",
      ["S"] = "open_split",
      ["s"] = "open_vsplit",
      -- ["S"] = "split_with_window_picker",
      -- ["s"] = "vsplit_with_window_picker",
      ["t"] = "open_tabnew",
      -- ["<cr>"] = "open_drop",
      -- ["t"] = "open_tab_drop",
      ["w"] = "open_with_window_picker",
      --["P"] = "toggle_preview", -- enter preview mode, which shows the current node without focusing
      ["C"] = "close_node",
      ["z"] = "close_all_nodes",
      --["Z"] = "expand_all_nodes",
      ["a"] = { 
        "add",
        -- this command supports BASH style brace expansion ("x{a,b,c}" -> xa,xb,xc). see `:h neo-tree-file-actions` for details
        -- some commands may take optional config options, see `:h neo-tree-mappings` for details
        config = {
          show_path = "none" -- "none", "relative", "absolute"
        }
      },
      ["A"] = "add_directory", -- also accepts the optional config.show_path option like "add". this also supports BASH style brace expansion.
      ["d"] = "delete",
      ["r"] = "rename",
      ["y"] = "copy_to_clipboard",
      ["x"] = "cut_to_clipboard",
      ["p"] = "paste_from_clipboard",
      ["c"] = "copy", -- takes text input for destination, also accepts the optional config.show_path option like "add":
      -- ["c"] = {
      --  "copy",
      --  config = {
      --    show_path = "none" -- "none", "relative", "absolute"
      --  }
      --}
      ["m"] = "move", -- takes text input for destination, also accepts the optional config.show_path option like "add".
      ["q"] = "close_window",
      ["R"] = "refresh",
      ["?"] = "show_help",
      ["<"] = "prev_source",
      [">"] = "next_source",
    }
  },

  filesystem = {
      window = {
      mappings = {
        ["<bs>"] = "navigate_up",
        ["."] = "set_root",
        ["H"] = "toggle_hidden",
        ["/"] = "fuzzy_finder",
        ["D"] = "fuzzy_finder_directory",
        ["f"] = "filter_on_submit",
        ["<c-x>"] = "clear_filter",
        ["[g"] = "prev_git_modified",
        ["]g"] = "next_git_modified",
      }
    }
  },

  buffers = {
    window = {
      mappings = {
        ["bd"] = "buffer_delete",
        ["<bs>"] = "navigate_up",
        ["."] = "set_root",
      }
    },
  },

  git_status = {
    window = {
      position = "float",
      mappings = {
        ["A"]  = "git_add_all",
        ["gu"] = "git_unstage_file",
        ["ga"] = "git_add_file",
        ["gr"] = "git_revert_file",
        ["gc"] = "git_commit",
        ["gp"] = "git_push",
        ["gg"] = "git_commit_and_push",
      }
    }
  }
})

-- nvim cmp
require('cmp').setup({
  mapping = require('cmp').mapping.preset.insert({
    ['<C-b>'] = require('cmp').mapping.scroll_docs(-4),
    ['<C-f>'] = require('cmp').mapping.scroll_docs(4),
    ['<C-Space>'] = require('cmp').mapping.complete(),
    ['<C-e>'] = require('cmp').mapping.abort(),
    ['<CR>'] = require('cmp').mapping.confirm({ select = true }),
    ["<Tab>"] = require('cmp').mapping(function(fallback)
      if require('cmp').visible() then
        require('cmp').select_next_item()
      elseif require('luasnip').locally_jumpable(1) then
        require('luasnip').jump(1)
      else
        fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
      end
    end, { "i", "s" }),
    ["<S-Tab>"] = require('cmp').mapping(function()
      if require('cmp').visible() then
        require('cmp').select_prev_item()
      elseif require('luasnip').locally_jumpable(-1) then
        require('luasnip').jump(-1)
      end
    end, { "i", "s" }),
  }),
})
