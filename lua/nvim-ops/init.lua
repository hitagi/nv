vim.api.nvim_set_option('mouse', 'a')
vim.api.nvim_set_option('dir', '/tmp')
vim.o.number = true
-- vim.o.relativenumber = true
vim.o.smarttab = true
vim.o.expandtab = true
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.linebreak = true
vim.o.hlsearch = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.autoindent = true
vim.o.spell = true
vim.o.wildmenu = true
vim.o.cursorline = true
vim.o.showmatch = true
vim.o.termguicolors = true
vim.o.autochdir = true
vim.o.syntax = on
vim.opt.list = true
-- vim.opt.listchars:append "eol:↴"
vim.opt.listchars:append "space:⋅"

vim.cmd [[
	set completeopt=menu,menuone,noselect,longest
	set clipboard+=unnamedplus
]]

vim.cmd [[ 
	highlight CursorLine cterm=bold ctermbg=black
  colorscheme tokyonight
]]

vim.cmd [[
	let g:VM_mouse_mappings = 1
	let g:markdown_fenced_languages = ['bash=sh', 'javascript', 'js=javascript', 'json=javascript', 'typescript', 'ts=typescript', 'php', 'html', 'css']
  let g:neo_tree_remove_legacy_commands = 1
]]
