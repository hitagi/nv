return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'
  use 'folke/tokyonight.nvim'
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-vsnip'
  use 'hrsh7th/vim-vsnip'
  use 'saadparwaiz1/cmp_luasnip'
  use 'rafamadriz/friendly-snippets'
  use 'ray-x/cmp-sql'
  use 'Exafunction/codeium.nvim'
  use 'potamides/pantran.nvim'
  use 'numToStr/Comment.nvim'
  use 'petertriho/nvim-scrollbar'
  use 'preservim/tagbar'
  use 'nvim-tree/nvim-web-devicons'
  use 'norcalli/nvim-colorizer.lua'
  use 'ixru/nvim-markdown'
  use 'lukas-reineke/indent-blankline.nvim'
  use 'nvimdev/template.nvim'
  use 'sbdchd/neoformat'
  use 'dense-analysis/ale'
  use 'onsails/lspkind.nvim'
  use {
    'nvim-lualine/lualine.nvim',
    requires = {
      'nvim-tree/nvim-web-devicons', opt = true,
    }
  }
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  use {
    'mg979/vim-visual-multi',
    branch = 'master'
  }
  use {
    "akinsho/toggleterm.nvim",
    tag = '*'
  }
  use {
    'CRAG666/code_runner.nvim',
    requires = 'nvim-lua/plenary.nvim'
  }
  use {
    'romgrk/barbar.nvim',
    requires = 'nvim-tree/nvim-web-devicons'
  }
  use {
    'nvim-neo-tree/neo-tree.nvim',
    branch = 'v2.x',
    requires = { 
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
      'MunifTanjim/nui.nvim',
    }
  }
  use {
    'nvim-telescope/telescope-fzf-native.nvim',
    run = 'make'
  }
  use {
  'nvim-telescope/telescope.nvim', tag = '0.1.2',
-- or                            , branch = '0.1.x',
  requires = { {'nvim-lua/plenary.nvim'} }
}

  use {
    'L3MON4D3/LuaSnip',
    tag = "v2.*",
    run = "make install_jsregexp"
  }

end)
